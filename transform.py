from typing import List, Tuple

import images
from images import size, create_blank


def change_colors(image: List[List[Tuple[int, int, int]]],
                  to_change: Tuple[int, int, int],
                  to_change_to: Tuple[int, int, int]) -> List[List[Tuple[int, int, int]]]:

    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            pixel = image[x][y]
            if pixel == to_change:
                new_pixel = to_change_to
            else:
                new_pixel = pixel
            new_image[x][y] = new_pixel

    return new_image

def rotate_right(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(height, width)

    for x in range(width):
        for y in range(height):
            new_image[y][width - 1 - x] = image[x][y]

    return new_image


def mirror(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_image[x][y] = image[width - 1 - x][y]

    return new_image

def rotate_colors(image: List[List[Tuple[int, int, int]]], increment: int) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            r, g, b = image[x][y]

            r = (r + increment) % 255
            g = (g + increment) % 255
            b = (b + increment) % 255

            new_image[x][y] = (r, g, b)

    return new_image
def blur(image: List[List[Tuple[int, int, int]]]) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            total_r, total_g, total_b = 0, 0, 0
            count = 0

            for dx in [-1, 0, 1]:
                for dy in [-1, 0, 1]:
                    nx, ny = x + dx, y + dy

                    if 0 <= nx < width and 0 <= ny < height:
                        tr, tg, tb = image[nx][ny]
                        total_r += tr
                        total_g += tg
                        total_b += tb
                        count += 1

            if count > 0:
                new_pixel = (total_r // count, total_g // count, total_b // count)
                new_image[x][y] = new_pixel

    return new_image

def shift(image: List[List[Tuple[int, int, int]]], horizontal: int, vertical: int) -> List[List[Tuple[int, int, int]]]:
    width, height = size(image)
    new_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            new_x = (x + horizontal) % width
            new_y = (y + vertical) % height
            new_image[new_x][new_y] = image[x][y]

    return new_image

def crop(image: List[List[Tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> List[List[Tuple[int, int, int]]]:
    new_image = []

    for row in image[y:y+height]:
        new_image.append(row[x:x+width])

    return new_image

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    grayscale = [[(sum(image[x][y])//3,)*3 for y in range(height)] for x in range(width)]
    return grayscale
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    rows = len(image)
    cols = len(image[0])
    filtered_image = []
    for i in range(rows):
        filtered_image.append([])
        for j in range(cols):
            new_red = min(int(image[i][j][0] * r), 255)
            new_green = min(int(image[i][j][1] * g), 255)
            new_blue = min(int(image[i][j][2] * b), 255)
            filtered_image[i].append((new_red, new_green, new_blue))
    return filtered_image































