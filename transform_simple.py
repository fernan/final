import sys
from transforms import *
from images import read_img, write_img
from PIL import Image

def main():
    if len(sys.argv) != 3:
        print("Uso: python transform_simple.py <imagen> <transformacion>")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = input_file.replace('.', '_trans.')

    transform_type = sys.argv[2]

    image = read_img(input_file)

    if transform_type == 'rotate_right':
        transformed_image = rotate_right(image)
    elif transform_type == 'mirror':
        transformed_image = mirror(image)
    elif transform_type == 'blur':
        transformed_image = blur(image)
    elif transform_type == 'greyscale':
        transformed_image = greyscale(image)
    else:
        print(f"Transformación no reconocida: {transform_type}")
        sys.exit(1)

    write_img(transformed_image, output_file)
    print(f"Transformación realizada y guardada en {output_file}")

if __name__ == '__main__':
    main()
