from transforms import rotate_colors, mirror, blur
from images import read_img, write_img
import sys

def main():
    if len(sys.argv) < 4:
        print("Uso: python transform_multi.py <imagen> <transformacion1> <parametros1> <transformacion2> <parametros2> ...")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = input_file.replace('.', '_trans.')

    image = read_img(input_file)

    i = 2
    while i < len(sys.argv):
        transform_type = sys.argv[i]
        i += 1

        if transform_type == 'rotate_colors':
            if i >= len(sys.argv):
                print("Faltan parámetros para rotate_colors.")
                sys.exit(1)
            increment = int(sys.argv[i])
            image = rotate_colors(image, increment)
        elif transform_type == 'mirror':
            image = mirror(image)
        elif transform_type == 'blur':
            image = blur(image)
        else:
            print(f"Transformación no reconocida: {transform_type}")
            sys.exit(1)

        i += 1

    write_img(image, output_file)
    print(f"Transformaciones realizadas y guardadas en {output_file}")

if __name__ == '__main__':
    main()
