from transforms import change_colors, rotate_colors, shift
from images import read_img, write_img
import sys
def main():
    if len(sys.argv) < 4:
        print("Uso: python transform_args.py <imagen> <transformacion> <parametros>")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = input_file.replace('.', '_trans.')

    transform_type = sys.argv[2]

    image = read_img(input_file)

    if transform_type == 'change_colors':
        if len(sys.argv) != 9:
            print("Uso incorrecto para change_colors. Deben ser 6 valores RGB.")
            sys.exit(1)
        params = tuple(map(int, sys.argv[3:]))
        transformed_image = change_colors(image, params[:3], params[3:])
    elif transform_type == 'rotate_colors':
        if len(sys.argv) != 4:
            print("Uso incorrecto para rotate_colors. Debe ser un valor entero.")
            sys.exit(1)
        increment = int(sys.argv[3])
        transformed_image = rotate_colors(image, increment)
    elif transform_type == 'shift':
        if len(sys.argv) != 5:
            print("Uso incorrecto para shift. Deben ser dos valores enteros.")
            sys.exit(1)
        params = tuple(map(int, sys.argv[3:]))
        transformed_image = shift(image, params[0], params[1])
    else:
        print(f"Transformación no reconocida: {transform_type}")
        sys.exit(1)

    write_img(transformed_image, output_file)
    print(f"Transformación realizada y guardada en {output_file}")

if __name__ == '__main__':
    main()
